(function () {
  "use strict";

  if (typeof window === "undefined") {
    return;
  }

  if (!window.vidium) {
    window.vidium = {};
  }

  var v = window.vidium;

  const pages = new (function () {
    let root;
    this.setCurrent = (name) => {
      [...root.querySelectorAll(".page")].forEach((el) =>
        el.classList.remove("active")
      );
      const page = root.querySelector(".page.page_" + name);
      if (!page) {
        this.currentName = null;
        this.current = null;
        return;
      }
      page.classList.add("active");
      this.currentName = name;
      this.current = page;
    };

    this.init = (el, current) => {
      el = v.util.domElement(el);
      if (!el) {
        el = v.util.domElement(".pages");
      }
      if (!el) {
        throw new Error(
          "parent pages element not found. pass element to this method or insert .pages class"
        );
      }
      this.root = root = el;
      if (current) {
        this.setCurrent(current);
        return;
      }
      const active = el.querySelector(".page.active");
      if (active) {
        const nameClass = [
          ...document.querySelector(".pages").querySelector(".page.active")
            .classList,
        ].filter((e) => e.startsWith("page_"))[0];
        if (nameClass) {
          this.setCurrent(nameClass.substr(5));
          return;
        }
      }
      this.setCurrent();
    };
  })();

  v.ui = {
    pages,
  };
})();
