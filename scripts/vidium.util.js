(function () {
  "use strict";

  if (typeof window === "undefined") {
    return;
  }

  if (!window.vidium) {
    window.vidium = {};
  }

  var v = window.vidium;

  v.util = {
    isElement,
    domElement,
  };

  // https://stackoverflow.com/a/384380/6391336
  function isElement(obj) {
    try {
      //Using W3 DOM2 (works for FF, Opera and Chrome)
      return obj instanceof HTMLElement;
    } catch (e) {
      //Browsers not supporting W3 DOM2 don't have HTMLElement and
      //an exception is thrown and we end up here. Testing some
      //properties that all elements have (works on IE7)
      return (
        typeof obj === "object" &&
        obj.nodeType === 1 &&
        typeof obj.style === "object" &&
        typeof obj.ownerDocument === "object"
      );
    }
  }

  function domElement(element) {
    // selector
    if (typeof element === "string") {
      return document.querySelector(element);
    }
    // native
    if (isElement(element)) {
      return element;
    }
    // jquery
    if (element && element[0] && isElement(element[0])) {
      return element[0];
    }
  }
})();
