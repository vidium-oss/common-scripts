(function () {
    "use strict";

    var elementContent = "<div id=\"ie\">\n" +
        "    <div class=\"ie_alert\">\n" +
        "        <div class=\"ie_alert_title\">\n" +
        "            Обращаем Ваше внимание, что для участия в мероприятии Вам необходимо использовать следующие браузеры - Chrome, Firefox, Safari, Opera, Edge.\n" +
        "        </div>\n" +
        "        <div class=\"ie_alert_subtitle\">Браузер Internet Explorer не поддерживается.</div>\n" +
        "    </div>\n" +
        "</div>"
    var styleContent = " #ie {\n" +
        "            display: none;\n" +
        "        }\n" +
        "\n" +
        "        #ie.show {\n" +
        "            position: absolute;\n" +
        "            display: block;\n" +
        "            width: 100%;\n" +
        "            width: 100vw;\n" +
        "            height: 100%;\n" +
        "            height: 100vh;\n" +
        "            background: #666666;\n" +
        "            flex-direction: column;\n" +
        "            align-items: center;\n" +
        "            justify-content: center;\n" +
        "            display: flex;\n" +
        "            z-index: 100;\n" +
        "            position: fixed;\n" +
        "            top: 0;\n" +
        "        }\n" +
        "\n" +
        "        .ie_alert {\n" +
        "            width: 450px;\n" +
        "            max-width: 100%;\n" +
        "            background: white;\n" +
        "            padding: 20px;\n" +
        "            text-align: center;\n" +
        "            line-height: 140%;\n" +
        "            border-radius: 4px;\n" +
        "        }\n" +
        "\n" +
        "        .ie_alert_subtitle {\n" +
        "            margin-top: 20px;\n" +
        "            color: red;\n" +
        "        }"

    // Get IE or Edge browser version
    var version = detectIE();

    if (version === false) {
        // NOT
    } else if (version >= 12) {
        // EDGE
    } else {
        // IE!
        showAlert();
    }

    /**
     * detect IE
     * returns version of IE or false, if browser is not Internet Explorer
     */
    function detectIE() {
        var ua = window.navigator.userAgent;

        // Test values; Uncomment to check result …

        // IE 10
        // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

        // IE 11
        // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

        // Edge 12 (Spartan)
        // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

        // Edge 13
        // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
    }

    function showAlert() {
        var elem = document.createElement('div');
        elem.innerHTML = elementContent;
        var style = document.createElement('style');
        style.innerHTML = styleContent;
        document.getElementsByTagName('head')[0].appendChild(style);
        document.getElementsByTagName('body')[0].appendChild(elem);
        document.getElementById('ie').className = "show";
        document.getElementsByTagName('body')[0].style = "overflow: hidden;"
    }
})();
