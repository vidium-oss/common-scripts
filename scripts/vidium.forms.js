(function () {
  "use strict";

  if (typeof window === "undefined") {
    return;
  }

  if (!window.vidium) {
    window.vidium = {};
  }

  var v = window.vidium;

  v.forms = {
    collect: collect,
    send: send,
    createSender: createSender,
  };

  v.users = {
    validate: validate,
    get: get,
    update: update,
    getKeyByEmail: getKeyByEmail,
  };

  async function get(eventCode, key) {
    if (!eventCode || !key) {
      throw new Error("eventCode and key must be both provided");
    }

    return await (
      await fetch(endpoint() + "/legacy/v3/getMe", {
        headers: {
          "x-event": eventCode,
          "x-user": key,
        },
      })
    ).json();
  }

  async function validate(eventCode, key) {
    try {
      return !!get(eventCode, key);
    } catch (e) {
      return false;
    }
  }

  function errorResult(message, type, extend) {
    return Object.assign(
      {
        error: true,
        type: type || "default",
        message: message || "unknown error",
      },
      extend
    );
  }

  function createSender(eventCode) {
    var isSending = false;

    return {
      send: async function (name, formData) {
        if (isSending) {
          return errorResult("already sending", "duplicate");
        }
        try {
          isSending = true;
          return send(eventCode, name, formData);
        } finally {
          isSending = false;
        }
      },
    };
  }

  function successResult(data) {
    return { error: false, data: data };
  }

  function collect(elementOrSelector) {
    var element;

    const domElement = v.util.domElement;

    if (typeof elementOrSelector === "undefined") {
      element = domElement("form") || domElement("body");
    } else {
      element = domElement(elementOrSelector);
    }

    if (!element) {
      return errorResult("form element not found", "tech");
    }

    return collectFormData(element);
  }

  function collectFormData(form) {
    var fields = form.querySelectorAll("input, select, textarea");
    var data = {};

    fields.forEach(function (elem) {
      var val = elem.value;
      var name = elem.name;

      var isCheckbox = elem.type === "checkbox";
      var isRadio = elem.type === "radio";
      var isCheckable = isCheckbox || isRadio;
      var isChecked = isCheckable && !!elem.checked;

      if (isCheckable && !isChecked) {
        return;
      }

      if (isCheckbox) {
        val = isChecked;
      }

      data[name] = val;
    });

    return successResult(data);
  }

  function collectRecoveryData(form) {
    var data = {};

    var fields = form.querySelectorAll("input, select, textarea");

    fields.forEach(function (elem) {
      // var block = elem.closest(".questions");
      var val = elem.value;
      var name = elem.name;
      var id = elem.id;
      var placeholder = elem.placeholder;

      var isCheckable = ["checkbox", "radio"].indexOf(elem.type) >= 0;
      var isChecked = isCheckable && !!elem.checked;

      var labelElement = id
        ? block.querySelector('label[for="' + id + '"]')
        : null;
      var label = labelElement ? labelElement.textContent.trim() : "";

      if (!data[name]) {
        var title = block.querySelector("h1").textContent.trim();
        data[name] = { answers: [], title, name };
      }

      var answer = { checked: val, label, value: elem.value };
      if (elem.value === "other") {
        var textElem = block.querySelector('input[name="' + name + '_other"]');
        if (textElem) {
          answer.text = textElem.value;
        }
      }
      if (answer.checked === false) {
        return;
      }
      data[name].answers.push(answer);
    });

    return data;
  }

  function endpoint() {
    if (v && v.config && v.config.apiEndpoint) return v.config.apiEndpoint;
    return "https://frame.niceplayer.ru/api";
  }

  async function update(eventCode, userKey, name, formData) {
    const config = {
      method: "put",
      headers: {
        "content-type": "application/json",
        "x-event": eventCode,
        "x-user": userKey,
      },
      body: JSON.stringify({
        name: name,
        formData: formData,
      }),
    };
    return await (
      await fetch(endpoint() + "/legacy/v3/updateMe", config)
    ).json();
  }

  async function send(eventCode, name, formData) {
    try {
      var apiEndpoint = endpoint() + "/legacy/";

      var method = "public/registerUser";

      var result = await fetch(apiEndpoint + method, {
        method: "post",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          eventCode: eventCode,
          name: name,
          formData: formData,
        }),
      });

      if (result.status != 200)
        return errorResult("request failed", "api", { response: result });

      return successResult(result);
    } catch (e) {
      return errorResult('inner error: "' + e.message + '"', "error", {
        error: e,
      });
    }
  }

  async function getKeyByEmail(eventCode, email) {
    var apiEndpoint = endpoint() + "/legacy/";
    var method = "public/findUserInfo";
    let data = null;

    try {
      let result = await fetch(apiEndpoint + method, {
        method: "post",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          eventCode: eventCode,
          data: { email: email },
        }),
      });
      if (result.status != 200) {
        let body = null;
        try {
          body = await ((result.headers.get("content-type") || "").indexOf(
            "application/json"
          ) >= 0
            ? result.json()
            : result.text());
        } catch (e) {
          console.error("Error fetching body", { error: e });
        }
        return errorResult("request failed", "api", {
          response: result,
          data: body,
        });
      }

      return successResult(result);
    } catch (e) {
      return errorResult('inner error: "' + e.message + '"', "error", {
        error: e,
      });
    }
  }
})();
